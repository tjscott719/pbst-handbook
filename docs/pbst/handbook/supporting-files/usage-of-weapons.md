# 🔫 Usage of weapons 
## PBST Specific Weapons
Loadouts for PBST members can be found close to the uniform givers, other givers may be spread throughout the facility. Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts.

These PBST weapons are only meant for PBST duties and may not be used for randomly killing anyone. Cadets abusing PBST Weapons are to be warned using the standard warning procedure (more info in the Rulebreakers chapter). If you find a Tier abusing PBST Weapons, collect evidence and report to an SD+.

Be sure to have your uniform on when taking PBST weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty. PBST weapons may never be used to cause a melt-or freezedown, unless TMS is hosting a reverse raid or when a Trainer gives a direct order.

## Weapons from other groups
PBST on duty may not use tools from other PB groups like PET or TMS. If you find a PBST member using weapons from another group, warn them according to the procedure in the Rulebreakers chapter.

## Other Weapons
If you are using non-PBST weapons on duty (like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned), you have to follow the same rules as if they were PBST Weapons.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. You can, for example, restrict rooms without needing permission, though excessive room restriction may count as mass random killing, which is always forbidden.

## How to deal with Rulebreakers
If a Security member is found breaking a rule in the handbook, alert them of their wrongdoing and give them a warning. If said Security member doesn’t listen or actually uses their weapons irresponsibly, kill them.

If a rogue PBST member is going way out of line, you can use the Comms server to rend a report of a handbook violation. Make sure you have evidence of the user breaking the rules, and being warned for it.

If a Tier is being abusive with their weapons, record the evidence and send it to an SD+.

## How to deal with Exploiters
Exploiting is using any external program to alter gameplay, like autoclicker, glitching and hacking. It is against Roblox ToS.

If you see an exploiter, call PIA with the `!call` command so they can be banned. If you can, record them exploiting so the PIA will have evidence to ban them. You are free to attack and kill the exploiters until they are banned. Should the exploiter leave before PIA arrive, give the recording to the PIA so that they still have the evidence they need to ban the exploiter.

## Room restrictions
Ranks below SD or authorized Elite Tier 2s are not allowed to “restrict rooms”, i.e. order non-PBST to leave an area of the game. Unless there’s a valid reason to engage (see above) or the room has a built-in lock, all rooms of the games are public access.

Elite Tier 2’s are only authorized to order room restrictions during TMS Raids. **They are not allowed to order room restrictions during patrols.**

## Kill on Sight (KOS)
Any person on KoS is to be killed immediately upon seeing them. TMS, exploiters, and mutants of PBCC are on KoS by default.  If you attack an exploiter, make somebody is recording it so the PIA will have evidence to ban them.

Only an SD or higher can place somebody on KoS for all PBST to follow. On-duty PBST may not put anyone on KoS unless permission has been granted by an SD+.

During TMS Raids, Elite Tiers can place KoS orders which last for the duration of the raid. An SD+ can override this order if they join. If no RRL is present in the raid, Elite Tiers can place raid KoS for them.

Specific actions can get you placed on KoS for all sides during TMS raids, more on that in the corresponding chapter.
